# I. LE PROJET FINAL 

![](images/FICHEA4_JURY.jpg)

## **LA LAMPE RUBAN - 135x135x300mm** 

![](images/Capture_d_écran_2021-01-20_à_22.19.50.png)

L’idée était de travailler la souplesse de la matière afin d’obtenir une forme courbe et homogène. La « **NASTRO CHAIR** » c’est une assise en fibre de verre blanche de forme organique et un pied rigide en acier. Ils fonctionnent ensemble outre leurs natures très différentes. Le principe de la « **LAMPE RUBAN** » est aussi très simple, **il s’agit de comprendre la fonction de l’objet afin de lui donner une identité claire aux yeux de tous**. 



![](images/Capture_d_écran_2021-01-20_à_18.36.24.png)

Une lampe c’est un pied et un abat-jour, deux choses bien distinctes qui fonctionnent aussi ensemble. **Un pied rigidifié par sa forme triangulaire pliée en deux permet de soutenir un abat-jour souple grâce aux propriétés inerte de la matière choisie** : le polypropylène transparent mat. Grâce à sa petite taille, l’abat-jour peut prendre une courbure plus dramatique qui renvoit au design italien de la « Nastro chair » . Le polypropylene ayant une épaisseur relativement fine, la lampe sera finalement plongée dans une résine epoxy afin d’être stabilisée. Ainsi, la lampe ruban prend tout son sens une fois allumée, grâce à son design homogène qui laisse apparaître ses formes courbes sous différentes opacités. 

``` DÉCOMPOSITION SCHÉMATIQUE DU FICHIER SVG : ```

![](images/Capture_d_écran_2021-01-20_à_18.55.25.png)

````
TEMPS - découpe : <1min. - sechage : 24h // PRIX TOTALE : 34,4€
````

# II. LA RECHERCHE


![](images/Capture_d_écran_2020-12-10_à_10.58.51.png)
```
Moulage de la chaise Nastro par CESARE LEONARDI - 1961
```

## Quelle démarche pour le projet final

Suite à la rencontre de jeudi sur teams, nous sommes amenés à choisir une maniere d'entreprendre la réalisation du projet final. Pour se faire, il faudra choisir parmis 5 démarches, 5 mots : LA RÉFÉRENCE, L'INFLUENCE, L'INSPIRATION, L'HOMMAGE, L'EXTENSION.

# LA RÉFÉRENCE, ʁe.fe.ʁɑɑs, fém.

Ainsi, avec plusieurs dictionnaires mis à notre disposition allant du dictionnaire contemporain voir "personnel", de par son coté déscriptif et libre à la modifications de chacun, allant jusqu'aux dictionnaires classiques avec des définitions à connotations plus littéraire voir premier degré, et finalement les plus "historiques" de la langue francaise qui amène à un sens plûtot encyclopédique des mots :

```En ce qui me concerne, cet exercice final sera mené par une démarche de l'ordre de la RÉFÉRENCE```

> PARMIS LES DIFFÉRENTES DÉFINITIONS, CELLES QUI SONT RESORTIES SONT :

1. WIKTIONNARY : Action de référer ou de renvoyer d’une chose à une autre, à un texte, à une autorité. "Vous vous sentirez des âmes de dandy en montant le superbe escalier Napoléon III qui mène vers le lounge proprement dit, appelé le Salon Rouge en référence à la couleur des banquettes aux formes origamiques. - Ouvrages de référence, ouvrages faits pour être consultés, tels que dictionnaires, recueils, etc. - "La référence qu’il donne est fausse."

2. LE PETIT ROBERT : en géométrie, système de référence : système d'axes par rapport auquel on détermine des
coordonnées - Fait de se référer (à un texte, une autorité, etc.). Faire référence à un auteur. Ouvrages de
référence, faits pour être consultés (dictionnaires, encyclopédies, etc). - AU FIGURÉ, Fait permettant de reconnaître la valeur de quelqu'un. 

3. LE PETIT LAROUSSE : Indication du passage d'un texte (page, paragraphe, ligne, etc.) permettant de s'y
reporter : Veuillez me fournir la référence de cette citation. - Action de référer, de renvoyer à un document, à une autorité: Indemnité fixée par référence
au traitement.

4. LE LITTRÉ : Action de se référer ou de renvoyer à un article, à un passage, à une chose ayant quelque rapport.-  Théorie de la formation des mots en sanscrit, en grec et en latin, avec références aux langues germaniques, F. Baudry, Gramm. comparée des langues classiques. - Ouvrages de référence, ouvrages que l'on consulte, tels que dictionnaires, recueils, etc.

# POURQUOI UNE TELLE DÉMARCHE ? 

La référence est pour moi, un moyen d'avancer avec un ``` tuteur ``` tout au long de l'exercice. Pour chaque idée qui émerge je pourrais regarder vers l'oeuvre de Léonardi et ainsi avoir le recul que j'ai pu avoir en reproduisant son travail lors du premier exercice. Par la même occasion, cela me permettra de faire face à la faisabilité de mon objet car celui-ci passera par differentes prises de décisions pour lesquelles j'aurais l'occasion de faire un parrallèle à quelque chose de réel qui existe de maniere physique. La référence est donc une aide personnel considérable à mon aprentissage.

La référence est renvoyée plusieurs fois à ```l'autorité```, en travaillant plus d'un mois sur le travail de Léonardi, j'ai découvert un artiste dont j'aime beaucoup la pensé et la maniere de travailler. En terme de Design, Leonardi est devenu une référence qui a donc pris de la valeur à mes yeux. La démarche de mon projet final se veut être d'autant mieux comprise par quelqu'un, une fois que la Nastro chair aura été consultée. Dans mon cas, la référence n'est donc pas l'objet en lui-même mais plutot l'artiste.

Ce rapport à l'homme et pas à l'objet m'a amener à me demander si il s'agissait alors d'une démarche relevant de hommage ou plûtot de la référence : 
À l'inverse de mes idées, l'hommage relève de la vénération et implique une position discraite voir effacée ; ce que moi je souhaites éléborer c'est un projet en étroite relation avec le travail de Léonardi sans pour autant qu'il se décalque totalement de celui-ci : Mon projet prendra une finalité indépendante et propre à sa conception pour avoir sa propre identité mais se verra d'autant plus compris une fois déposé au coté du design de la Nastro chair.

# LA RÉFÉRENCE, L'ARTISTE 
## CESARE LEONARDI - 1935
![](images/LEONARDI.jpg)

Aussi appelée la chaise ruban de par son esthétique en anneau épais et ondulé, la chaise Nastro a été pensée en 1956 par Leonardi et Stagi. Ils deviendront ensuite associés et ouvriront l'atelier Leonardi-Stagi. Cesare Leonardi se tourne ensuite vers d'autres pratiques artistiques notament le photographie et publiera son premier livre apres 20 ans de recherches ; L'Architettura degli Alberi où il y dévoile son obsession pour la nature par un nombre impressionant de dessins détaillés d'arbres.

Quand je parle d'une posture étant de l'ordre de  "la référence", c'est donc plus particulierement à Léonardi que je me réfere et à sa maniere de travailler. A l'instar de ce-dernier, je décide de continuer à travailler la forme dans la continuité de mon aprentissage lors de l'exercice de la lampe, avec les capacités intinsèques de la matière.

Dans l'idée de concevoir un projet en étroite relation avec l'objet, je souhaiterais concevoir un objet qui accompagnera la Nastro chair. Ainsi, dans cette notion "d'accompagnement", cette piece reprendra plusieurs similitudes afin d'évoquer un parralélisme avec la pièce maîtresse. 
Celles-ci sont : 
1° **La déformation de la forme par une force extérieur.** 
2° **Le contraste entre organicité et rigidité.** 
3° **L'homogonéité du design.**

![](images/Capture_d_écran_2020-12-08_à_16.58.01.png)

# A. PREMIERE IDÉE
## Une table d'appoint ?

Après la semaine projet, je décide de travailler sur une table d'appoint en carton afin de travailler sur une matiere accessible et peu chère ; les 3 faces de la table seront fabriquées à partir de planches de carton alvéolé découpées 18mm par la Lasersaure. Finalement, les 3 faces seront assemblées par 2 types d'articulations qui à l'inverse seront imprimés en 3D via la PrusaSlicer. 
![](images/Capture_d_écran_2020-12-08_à_17.10.04.png)
``` **! ATTENTION NE PAS UTILISÉ DU CARTON ALVÉOLÉ AUSSI ÉPAIS AVEC LA LASERSAURE : SON ÉPAISSEUR CRÉE UN APPEL D'AIR ET PREND FEU !** ```

```UPDATE Mercredi 09/12 : Je doute fortement de la qualité de l'objet par rapport à mes ambitions. Après avoir eu une correction avec Hélene et Gwendoline, je me rends compte que cette premiere idée ne colle pas avec les intentions premieres que j'avais envers ma réference ; La Nastro est pour moi, un objet simple et épuré de par sa conception homogène et unitaire. Or, cette premiere esquisse ne releve pas de la simplicité, mais d'un systeme d'emboitement contraignant & complexe, sa conception contredit les propos expliqués ci-dessus. Je vais donc reprendre l'exercice final depuis le début... ```

# B. RETOUR EN ARRIÈRE
## Moodboard

![](images/MOODBOARD.jpg)

La particularité du travail de Leonardi est d'expérimenter le design par la manipulation de l'objet et donc de la matiere, dans ma démarche (la référence) je décide d'explorer ses différentes oeuvres puis de les mêler avec d'autres reférences qui me viennent à l'esprit. 

_CE QUI EN RESORT EST :_

``` D'une part, l'influence de la nature sur l'objet ; la force du vent, l'erosion, la transformation par la chaleur, le moulage sur la terre, la thigmomorphogenèse, l'aphenphosmophobie. D'autre part la force de l'homme sur l'objet, torsion, compression, traction, déformation. La question est donc : En considérant les differentes forces possibles, comment vais-je déformer la matière ? ```

## C. DEUXIÈME IDÉE

![](images/Capture_d_écran_2020-12-10_à_16.21.20.png)


![](images/Capture_d_écran_2020-12-10_à_16.15.40.png)


![](images/Capture_d_écran_2020-12-10_à_16.11.57.png)

# D. TROISIÈME IDÉE
## Essais papier ...

![](images/IMG_4277.JPG)

Je décide de retravailler sur base de prototype en papier comme lors de l'exercice de la lampe car c'est là que j'ai le plus appris sur le design de l'objet. Je reproduis la Nastro, je la déforme, je l'exagère ..
Mon choix se fait pour cet essai, l'idée est d'exagérer le principe de la Nastro en tirant au maximun sa courbe. Cette étape me permet d'avoir une premiere jonction qui vient solidifier l'objet. Pour cela, je joue sur des fentes de chaques cotés. En créant une incision au sommet, j'ai la possibilité d'insérer un plateau qui laisse apparaître la fonction de l'objet. Finalement, le plateau est rigidifié en se poursuivant en pied, lui-même formé de deux obliques. On retrouve donc un pied étroitement lié à la Nastro qui est ensuite connecté à un plateau en angle, ainsi j'obtiens le contraste entre courbe & rigidité.

![](images/Capture_d_écran_2020-12-16_à_20.35.24.png)

```Pour aboutir à l'echelle 1:2 J'ai du cinder la première pièce en deux. Pour cela je dois joindre celles-ci, je joue sur une fente présente sur les deux pièces et où passera la dernière pièce (le plateau) ce qui devrait solidifier le tout. J'ai aussi imprimer la deuxieme pièce en double afin de l'épaissir. Les deux pieces sont donc dépendantes l'une de l'autre afin de d'arriver à un objet homogène et solide.```

![](images/Capture_d_écran_2020-12-16_à_22.09.51.png)

# E. PREMIER RÉSULTAT (1:2)

![](images/Capture_d_écran_2020-12-16_à_19.09.09.png)

En terme d'esthétique, le résultat rappelle le design italien aux courbures "dramatiques". OUI, MAIS : l'expérimentation de l'objet m'a appris plusieurs choses. Tout d'abord, au niveau de sa fonction : une partie du design ne prend pas toute son utilité, les courbes de l'objet permettent de rigidifié le pied et de soutenir le plateau. Cependant, les circonférences de celles-ci prennent beaucoup de place et n'apportent pas plus de surface quant à la possibilté d'y poser quelques choses. Finalement, l'objet reste très complexe en terme de lisibilité et ne correspond donc pas à mes attentes.. 

``` Après l'échange du pré-jury, je pense tout simplement que l'objet ne correspond ni aux outils mis à ma disposition, ni à ma maniere de travailler. J'en conclus que l'objet qui correspond le mieux au polypropylene reste la lampe de l'exercice précédent.```

![](images/Capture_d_écran_2020-12-16_à_21.21.28.png)

## F. RÉFLEXION POUR LA SUITE 

Les problèmes que je rencontre à chaques fois avec la table d'appoint sont ses dimensions. En effet, en travaillant avec la matiere (sans systeme d'attache) je suis limitée par les dimensions de la lasersaure (122x61cm). Après ce premier résulat, je me demande si l'idée d'imaginer une "collection" de design ne serait pas plus approprié afin de poursuivre la réflexion sur la référence, je m'explique :

Afin d'aboutir le travail sur papier et le concept du "ruban", je souhaiterais créer une collection d'objets liés les uns avec les autres ainsi qu'avec la Nastro. En travaillant les concepts j'ai des idées de d'objets à plus petites échelle, notamment la lampe (ex n°4), cette fois-ci concue de manière aboutie.

![](images/LAMPE.jpg)

```
IMAGES TIRÉES DE L'EXERCIE IV
```

## G. PROTOTYPE PAPIERS

```La lampe est composée de deux pièces qui s'emboîtent. Le pied permet la stabilité par la rigité du pliage et l'abat-jour utilise la résistance de la matiere afin de former une courbe. Ils sont ensuite liés ensemble par deux fentes placés sur l'abat-jour qui viennen ensuite s'insérées dans l'axe du pied partiellement ouvert. C'est un design simple qui est monté instantanément une fois découpé,il peut être reproduit et compris rapidement, cela se reflète dans la forme épurée de l'objet.```

![](images/PROTOTYPEPAPIER.jpg)

## H. PROTOTYPE POLYPROPYLENE 1

![](images/prototype1.png)

Je décide de travailler sur un modele plus grand, plus massif, mais le résultat n'y est pas tout à fait.. La courbure de l'abat-jour doit etre réduite car on perd le design fin et léger du prototype de départ. L'ampoule reste très voyante (trop grande) et je ne trouve pas de solution à cela mis a part changer le modèle : passer sur une ampoule E14 plutot qu'E27? Finalement, la lampe reste très fragile (voir instable) à cause de l'épaisseur du propylene qui est très fine (1mm). Je ne trouve pas d'épaisseur supérieur sur le marché, je pense donc à 2 possibilités ; soit superposer 2 épaisseurs de pied qui seront fixés par la 3e piece (l'abat-jour) ou bien je change de matiere pour le pied (plexiglas?) mais cela risque de paraître inesthétique et donc de s'éloigner de mes attentes.

Suite à l'échange de ce matin, je décide de rester sur un petit modele après avoir vu les proportions du modele Sissi de chez Flos, imaginée par Philippe Starck en 1991, je décide de 3 choses :

1. **partir sur une lampe d'une hauteur max de 30 cm. - Renforce le principe d'un design épuré.**
2. **partir sur une ampoule LED de 2 watt - conforme à la taille de l'abat-jour.**
3. **créer un socle pour solidifier la lampe - le poids de la lampe étant trop léger.** 

## I. VARIANTES

![](images/Capture_d_écran_2021-01-20_à_18.27.46.png)

J'ai retravaillé l'inclinaison et augmenter la taille de l'abat-jour afin d'oculter au mieux l'ampoule LED. J'ai aussi penser une deuxieme proposition ou le pied est plus "simple" ou l'ancrage au socle n'est pas élargi mais je préfere l'esthétique du premier pied plus évasé en bas.

# III. RESULTAT FINAL

![](images/Capture_d_écran_2021-01-20_à_19.54.41.png)

1. **La lampe LED choisie n'existant pas sur le marché il faudra la connecter soi-même, j'y suis arrivée grâce à l'aide d'un ami électricien. Pour cela, il faut une douille LED composée de 2 fils reliés à 2 x WAGO (2 entrées) : il faut insérer le premier fil dans le premier wago et le deuxieme fil dans le deuxieme wago, finalement les 2 fils de la prise sont connecté aux deux entrées restantes dans le premier et le deuxieme wago. En fait, le wago est la connexion entre la douille LED et la prise. Pour une question d'esthétique j'ai fais en sorte de passer les deux WAGOS devant et les 2 fils derrières. Je trouvais d'ailleurs interessant le coté "rationnel" de la lampe qui montre son mécanisme électrique grâce à la transparence du matériau, ce résultat était inatendu mais c'est quelque chose que je garderais par la suite car cela reflète la clarté du design de la "LAMPE RUBAN".** 
2. **J'ai finalement fait un socle en époxy coulé dans un moule en silicone. Pour cela vous devez bien mélanger 1 dose de durcisseur (100ml) pour 2 doses de résine (200ml) ensemble. Une fois mélanger, il faut le couler dans le moule et y insérer la lampe. Après 24H, le socle peut être démoulé.**

# IV. PERSPECTIVES POUR LA SUITE

![](images/Capture_d_écran_2021-01-20_à_18.36.24.png)

La lampe ruban est pour moi un prototype, je souhaiterais à l'avenir travailler sur différentes variantes de lampes en polypropylène et ainsi aboutir à une réelle collection dans le thème du "RUBAN". Avant cela, il y a diverses améliorations que j'ai déjà en tête et qui m'amène à interroger le résulat final sur sa finalité :

1. **Le socle est à retravailler afin de rentrer completement dans l'homogénéité du design - épaisseur plus fine OU un systeme de rigidification de la base uniquement en trampant le pied dans la résine epoxy?**
2. **Je souhaiterais retravailler le fil électrique, j'apprécie le coté "honnête" du mécanisme voyant, je souhaiterais d'autant plus l'affirmer par un fil rouge qui donnerait une identité d'autant plus forte à la lampe.**

![](images/Capture_d_écran_2021-01-20_à_20.25.27.png)

Grâce à ce projet, **j'ai appris à concevoir la simplicité mais je pense aussi que le design est principalement dans le détail** : Ce projet est pour moi l'aboutissement de mon parcours au sein de l'option DESIGN car il m'a appris beaucoup de choses en terme d'outils numériques evidemment mais j'ai aussi appris sur moi-même par ce projet final. Je suis passée par pas mal de détours avant de revenir à l'idée la plus simple et la plus intuitive que j'ai eu au cour de ce quadrimestre. C'est finalement en m'écartant le plus de mes objectifs que je suis revenue à l'essentiel et que le choix de ma démarche m'a le plus aidée (la référence). En effet, en passant par plusieurs échecs en terme d'idées d'objet, je suis finalement revenue à la source, à cette image où l'on voit Leonardi travailler la déformation de la chaise NASTRO, à mes premiers prototypes en papier qui m'ont amenés à ce résultat final. A l'instar de la Nastro, la lampe ruban a donc la particularité d'avoir été pensée dans la réalité et non sur un programme de modélisation.

![](images/essaifinal.jpg)

![](images/Capture_d_écran_2021-01-20_à_22.37.50.png)















