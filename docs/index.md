![](images/avatar-photo.jpg)

## QUI SUIS-JE ?

Bonjour! je m'apelle Noémie, je suis étudiante en première année de master à la faculté d'Architecture, **la Cambre-Horta (ULB)**. Afin de me perfectionner dans les outils numériques, j'ai opté pour l'option _"Architecture & Design"_ durant ce semetre & c'est via cette page web que je documenterais mon parcours tout au long de mon aprentissage. 

C'est à l'adolescence que j'ai découvert un attrait pour l'**Art**. Sensible aux détails, à la photographie, à la musique et au cinéma, j'ai ensuite fais le choix d'entreprendre des études d'architecture. Après avoir passée mon enfance à la campagne dans les Ardennes belges, j'ai vécu à Liège durant mes années secondaires. J'ai finalement déménagé à Bruxelles en 2016 afin de découvrir une nouvelle ville lors de mes études supérieurs.

## _LA NASTRO CHAIR_ / **1961**

![](images/sample-photo.jpg)

Elle fut le point de départ dans la carrière de Leonardi, au même moment où il expérimente la flexibilité du maillage polygonal modulaire, la chaise Nastro a été pensée en 1956 par deux étudiants ; Leonardi et Stagi. Le premier prototype était fait de scagliola et de treillis métallique rendant son déplacement impossible. En 1960, un deuxième prototype sera créé en carton condensé et un brevet sera finalement déposé en 1964. 

La demande et la recherche de nouveauté de l’après-guerre marquera le lancement de l’esthétique de l’exces aux lignes radicales et théatrales en Italie. La production industrielle de la chaise abouttira finalement a l’utilisation de la fibre de verre tandis que la base en acier restera en acier chromé et poli renvoyant aux conceptions antérieures & laissant son design a jamais associé aux années 50. Les nouvelles techniques et les nouveaux matériaux feront de cet objet de design le centre d’attention de la sociéte des années 60. 

## POURQUOI?

Sa forme organique reflète une adaptation au corps humain et fut la première raison de mon choix ; Son apparence de ruban qui se réhausse et puis s’aplatit forme une assise inclinée qui permet de laisser reposer le poids du dos contre le dossier. L’objet en lui-même semble voler au dessus de sa base en acier grâce au système de porte-a-faux et aux différentes épaisseurs des matériaux. Le contraste suscité par le jeu d'ombres et l'aspect brillant des matériaux utilisés donnent à l’objet, un design épuré et légé. Son rapport au sol très bas suscite l’interrogation quant à sa fonction ; est-ce une sculpture, une chaise, un fauteuil? Finalement, ce qui fait l'identité forte de la chaise Nastro c'est son design qui renvoit à la période de la conquête spatiale et se distingue des autres pieces par sa silhouette à la fois intriguante et dramatique.


