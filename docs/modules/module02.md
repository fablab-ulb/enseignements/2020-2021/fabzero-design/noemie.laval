# II. DESSIN CAO - Fusion 360

Premier contact avec l'objet, **l'observation** ; Afin de comprendre la Nastro chair, je l'analyse en tournant autour de celle-ci. 
**Son principe** : un large ruban replié sur lui même auquel vient s'insérer un pied ; Ainsi, la chaise se caractérise par un jeu de courbes qui suscite à son tour un jeu d'ombres. Pour comprendre ses courbes je redessine l'objet coté :

 ![](../images/Capture_d_écran_2020-10-27_à_12.16.09.png)

Afin de modéliser l'objet, j'ai d'abord voulu procéder à son élaboration en travaillant de maniere symmétrique. Pour cela j'ai dessinée l'esquisse de la courbe que le profil de l'objet suit via l'outil Créer > Balayage (fig. A). Résultat peu concluant car la Nastro Chair releve d'un travail plus complexe en terme de courbes... 

> **CONCLUSION** : Le systeme d'esquisse 2D ne donne pas assez de fonctionnalités pour représenter le principe de "torsion" retrouvé dans le design de la Nastro chair. 


![](../images/sample-pic-2.jpeg)

J'ai donc finalement travaillé avec l'outil _"Créer une forme"_ qui permet de travailler directement en 3D. De maniere synthétique, voici la maniere dont j'ai procédé : 
1. La base se fait à partir d'un cylindre (fig. B) 
2. Grâce à l'outil _"transformation"_ je déforme le cylindre, 
3. Grâce à l'outil _"Pivot"_ je génere une torsion, 
4. Grâce aux déplacements des coordonnées de chaques points j'obtiens le resultat final. (fig. C) 

De maniere plus détaillée, je vous invite délors à lire la suite :

## PHASE 1 / CREER UNE FORME

La premiere phase consiste à adapter le cylindre afin d'obtenir une base comme point de départ pour obbtenir le design de la Nastro Chair. Dans la section Modifier 

![](../images/1+2.jpg)

![](../images/3+4.jpg)

![](../images/5.jpg)

## PHASE 2 / ADAPTER LES COURBES

> **/!\ Chaque modification est faite de maniere symmétrique, il est donc essentiel de documenter les manipulations faites à droite et d'ensuite les répéter a gauche.**

Je commence par adapter le dossier à une premiere inclinaison ; Je sélectionne les 2 courbes centrales sur les 4 premiers "niveaux" et les descends (via la flèche verticale qui s'affiche) en fonction de la pente souhaitée (A,B,C,D). Je remonte l'axe centrale en selection multiple afin de créer une assise plate (E). Ensuite, il faut "décaler" le dossier afin de laisser apparaître l'assise (F) ; pour l'ajustement, je n'utilise plus les lignes mais les coordonnées (G,H). D'abord pour le dossier, ensuite pour les accoudoirs (I). J'agrandi le dossier en remontant les points aux extremmités supérieures (J). J'adapte les coordonnées du dossier afin d'obtenir une oblique croissante et pas une courbe (K). Pour finir, je suréleve cette même oblique par la sélection multiple afin d'obtenir l'effet "bombé" des accoudoirs.

![](../images/phase2.jpg)

## PHASE 3 / SCULPTER LA FORME

Le design de la Nastro Chair est basé sur une forme souple et unitaire, celle du ruban. Sa conception 3D implique une analyse visuel pour pouvoir ensuite la redessiner, il n'existe donc pas un procédé précis selon moi mais plutôt un aprentissage du regard en parrallèle à la capacité de comprendre comment celle-ci fonctionne en tant que chaise. La troisième phase consiste donc à déplacer les coordonnées du modèle afin d'obbtenir le résultat final : Pour cela, je me positionne sur les 6 faces du cube (en haut à droite) une par une et déplace les coordonnées en m'aidant du quadrillage en arriere plan. Pour cette étape, je garde des images de la Nastro chair sur le coté afin de la reproduire au mieux. 

![](../images/phase3.jpg)

## PHASE 4 / FINALISATION

L'assise est inclinée afin de pouvoir s'asseoir en laissant reposer le poids du dos contre le dossier ; Pour cela il faut donc effectuer une rotation de 20° en selectionnant l'entiereté du modèle (A). Il faut ensuite ajouter une épaisseur à l'esquisse dans : Modifier > Epaissir, je donne une epaisseur de 1,5 mm (B). Pour finir, j'ai effectué un rendu en ABS (blanc) pour que le modèle prenne une dimension semblable à la fibre de verre comme dans la réalité (C,D).

![](../images/PHASE4.jpg)
