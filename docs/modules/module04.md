# IV. DECOUPE LASER - Lasersaure

Ce jeudi nous avons été inités à la formation de découpe numérique via une découpeuse laser : La lasersaure, une machine open-source qui a une puissance de 100W & est dotée d’un laser de type tube CO2 infrarouge. Sa surface de découpe est de 122x61cm avec une hauteur de 12cm. La vitesse maximale de découpe est de 6000mm/min. max (Eviter de monter jusqu’à cette vitesse car le temps que la machine l'atteigne, la découpe sera presque terminée & elle ne sera pas homogène). 

![](../images/lasersaur.jpg)

On trouve aussi une plus petite decoupeuse laser au fablab : la Muse FullSpectrum. Elle n'a pas été utilisée durant cet exercice mais voici les informations à savoir quant à son utilisation : Sa surface de découpe est de 50 cm x 30 cm avec une hauteur maximale de 6 cm, une puissance de 40 W & un laser de type tube Co2 (infrarouge). Il y a également un pointeur rouge supplémentaire afin de permettre un positionnement plus précis.

## CONCERNANT LES MATÉRIAUX UTILISÉS :

| RECOMMANDÉS | DÉCONSEILLÉS | INTERDITS |
| ------ | ------ | ------ |
| Bois contreplaqué (plywood,multiplex) | MDF, fumée épaisse,nocive à long terme | PVC, fumée acide,très nocive |
| Acrylique (PMMA,Plexiglass) | ABS, PS, fond facilement,fumée nocive | Cuivre, réfléchit totalement le laser |
| Papier, carton | PE, PET, PP, fond facilement | Téflon (PTFE), fumée acide,très nocive |
| Textiles | Composites à base de fibres, poussières très nocives | Vinyl, simili-cuir, peut contenir de la chlorine |
| / | Métaux, impossible à découper | Résine phénolique, époxy, fumée très nocive |

## PRÉCAUTIONS

- [x] Connaître le matériau utilisé et ses composants (VOIR TABLEAU!)
- [x] Ouvrir la vanne d’air comprimé
- [x] Ouvrir l’extracteur de fumée
- [x] Connaitre les bouton d’arrêt d’urgence 
- [x] Avoir un extincteur au CO2 à proximité

## ESQUISSE 

L'ÉNONCÉ - Imaginer une lampe à partir de l'objet initial. - 
Autrement dit, une lampe influencée par le design de la Nastro chair. Pour se faire, il faudra experimenter la faisabillité du modele sur des prototypes en papier. Finalement, la lampe sera découpée sur une feuille de polypropylene (un polymère thermoplastique semi-cristallin de grande consommation) & au format A2.

Je travaille sur une premiere idée en sablier afin d'évoquer la forme arrondie et la symmetrie des deux accoudoirs, je voulais aussi imaginer un 
premier systeme d'emboîtement. J'effectue une découpe à petite echelle pour tester et me rend vite compte que pour ce systeme d'attache, il faudra soit repenser leurs formes soit (et surtout) les agrandir..

![](../images/essai1.jpg)

Peu satisfaite du résultat, je décide d'abandonner cette idée. En effet, je ne retouve pas de similitudes entre la forme du premier prototype et la forme esthétique qui m'a plu dans la nastro chair & je décide donc de me remettre au travail : En repensant le design initial, je m'inspire du processus qu'a imaginé Leonardi ; une forme simple (le cylindre) qui subit diverses transformations : torsion, écrasement et rotation.
J'imagine donc des formes qui s'emboiteraient ensemble. De maniere plus précise ; une bande de propylene lié à ses extremmités qui a pour résultat un cylindre, un triangle avec des pieds, des éléments d'articulation suscités par le pivot, le pliage, ou encore la fente et son orientation... Structurellement, il faudra aussi penser au moment, aux forces réparties ansi que leurs sens : comment éviter que cette lampe ne bascule d'un coté ou de l'autre? 

![](../images/esquisse.jpg)

## PROTOTYPE

À partir d'une feuille de papier, je décide de travailler cette dernière idée de lampe. Je souhaite travailler la souplesse à l'instar de Cesare Leonardi, une forme courbe et homogène. Je veux aussi travailler sur le contraste ; La Nastro chair c'est une assise en fibre de verre blanche de forme organique et un pied rigide en acier. 
> Ils fonctionnent ensemble outre leurs natures très différentes.. Je décide de travailller sur 2 choses bien distinctes, un pied et un abat-jour qui fonctionneront également ensemble : 

![](../images/PHOTOLAMPE.jpg)

## INKSCAPE

Le modèle de la lampe est dessiné via un outils de conception au choix (AutoCAD, Fusion360,..) dans lequel lignes bleus = découpe & lignes rouges = gravure (pliage). A savoir que pour la découpe laser, on travaillera en dessin vectoriel, dessins qui auront la capacité de s'agrandir a l'infini sans perdre en qualité. On procede ensuite au lancement de découpe qui se fait via l’application Driveboardapp, déjà présente sur l’ordinateur du fablab :

1. Le fichier doit être importé en SVG (ou en DXF).
2. Une fois la clé USB insérée, on ouvre le fichier dans Inkscape : Séléction de l'objet > clic droit > Ungroup. Ceci va convertir les pixels en lignes vectorielles. On vérifie l'échelle du document en axes X & Y. 
3. Une fois terminé, on exporte le fichier en format SVG.

``` Attention à l'épaisseur de trait du dessin! J'ai eu un problème lors de la première découpe : les traits étaient trop fins et surtout introuvables pour le Drivebboardapp car laissés dans la couleur Ducalque et non en noir, j'ai donc modifié cela sur autocad en augmantant l'épaisseur de trait (15mm) et en les mettant en bleu. ```

![](../images/designlampe.jpg)

## DRIVEBOARDAPP

4. Installer le matériau dans la machine
5. Ouvrir le fichier DFX ou SVG dans DriveboardAPP
6. Pour remettre le laser à sa position de départ : Home
7. Déplacer la tête du laser OU la feuille de polypropylene en verifiant le cadrage via le bouton OFFSET
8. Régler la distance focale, la hauteur entre lentille et matériau (support suggéré : 15 mm)
9. Attribuer aux couleurs des lignes une vitesse (F) je laisse a 1500 par défaut & une puissance (%) j'introduits 30 pour une découpe, je divise en 2 pour le pliage, c-à-d 15.
10. Mettre les lignes dans l'ordre de découpe : pass 1,2,3,.. (Mettre les gravures rouge en premier pour éviter que la feuillle de propylene ne bouge).
11. Vérifier que le bouton "Status" est en vert. (Sinon clic gauche pour voir d'où vient le problème.)
12. "RUN"

``` NE PAS OUBLIER : Éteindre la machine (bouton rouge), la panne d’air comprimé, le refroidissement à eau (bouton noir), les extracteurs de fumée (boutons verts) ```

## RÉSULTAT DE DÉCOUPE

![](../images/essaifinal.jpg)
```La lampe est composée de deux pièces qui s'emboîtent. Le pied permet la stabilité par la rigité du pliage et l'abat-jour utilise la résistance de la matiere afin de former une courbe. C'est un design simple qui peut être reproduit et compris rapidement, je trouve que cela se reflète dans sa forme épurée.```

## RÉSULTAT FINAL

La lampe tient, je suis satisfaite du résultat. Afin d'améliorer son aspect pratique je rajouterais deux fentes sur la piece de l'abat-jou pour y insérer une bande de polypropylene permettant ainsi de soutenir l'ampoule. Celle-ci sera ensuite reliée à un fil qui passera dans la fente du pied (déjà présente). En ce qui concerne l'aspect esthétique, j'agrandirais l'échelle du modèle afin d'oculter la totatilité d'une ampoule de taille standard. 
``` Ici le résultat avec un systeme de petite ampoule à piles : ```

![](../images/LAMPE.jpg)






